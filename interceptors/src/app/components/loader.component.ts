import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoaderService } from "../services/loader.service";
import { load } from "@angular/core/src/render3";

@Component({
  styleUrls: ["loader.component.css"],
  templateUrl: './loader.component.html',
})
export class LoaderComponent {
  response;

  constructor(private http: HttpClient, public loaderService: LoaderService) { }

  run() {
    const url = "https://jsonplaceholder.typicode.com/albums/1";
    this.http.get(url).subscribe(r => (this.response = r));
  }
}
