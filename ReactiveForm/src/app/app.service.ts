import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class AppService {

  constructor (private httpClient: HttpClient) {}

  reporte: any;
  Rload(reporte) {
  const headers = new HttpHeaders();
  headers.append('Content-Type', 'application/json');
  return this.httpClient.get('http://localhost:3000/routes/reporte', { headers: headers })
  .map(res => res);
  
  }

}
