import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import {AppService} from '../app.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  reporte: String;
  public reportes: Array<any> = [];

  form = new FormGroup({
    cities: new FormArray([
      new FormControl([null]),
    ]),
  });
  get cities(): FormArray {
    return this.form.get('cities') as FormArray;
  }
  addCity() {
    this.cities.push(new FormControl());
    var array = this.reportes;
    var reportes;
    this.form.value.cities.forEach(function (element) {
      console.log(element)
      console.log(array);
      array = array.filter(item => item !== element)
    });
    this.reportes = array;
  }

  constructor(private Service: AppService ) { }

  ngOnInit() {

    const reporte = {
      reportes: this.reporte
    };
    console.log(reporte);
    this.Service.Rload(reporte).subscribe((data: any) => {
      console.log(data);
      if (data.success) {
        this.reportes = data.mainReportes;
        console.log('Ready Drop down for Reporter');
      } else {
        console.log('Fail Drop down for Reporter');
      }
    });
    
  }

  onSubmit() {
    console.log(this.cities.value);  // ['SF', 'NY']
    console.log(this.form.value);    // { cities: ['SF', 'NY'] }
  }
}