import axios from 'axios';
import config from '../Config/development';

const api = axios.create({
    baseURL: config.apiTarget,
    
});

api.interceptors.request.use(
    (axiosConfig) => {
        return {
            ...axiosConfig,
            headers: {
                ...axiosConfig.headers,
                'Content-Type': 'application/json',
            },

        };
    },
    error => Promise.reject(error),
);

api.interceptors.response.use(
    (response) => {
        switch (response.status) {
            case 200: case 201: case 202: case
                204:
                return response.data;
            default:
        }
        return response;
    },
    (error) => {
        const {
            response: errorResponse,
        } = error;
        switch (errorResponse.status) {
            case 400: case 401: case 404: case
                409:
                return errorResponse.data;
            case 500: case 502:
       
                return errorResponse.status;
            default:
        }
    },
);

export default api;
