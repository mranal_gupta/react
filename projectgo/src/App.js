import React, {Component} from "react";
import { Routing } from "./Components/Router";
import  Login  from "./Container/login.container";


class App extends Component {
  render() {
    return (
      <Routing >
     <div style={{ padding: 15 }}>
      <Login />

    </div>
    </Routing >
    );
  }
}

export default App;
