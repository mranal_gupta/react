module.exports = {
  env: process.env.NODE_ENV,
  apiTarget: "https://dev.gosimplevat.co.uk/api/",
  urlRoot: {
    Signup: "signup/",
    Signin: "signin/",
    Get: "my-vat/",
    Adduser: "vat/",
    Forget: "account/password/reset/",
    Welcome: "myprofile/",
    Dashboard: "dashboard/",
    Changepass: "change-password/",
    MTD: "vat/",
    Liab: "vat/",
    SubsGet: "subscription/",
    Delvalue: "vat/",
    Payment:"vat/",
    Obligation:"vat/",
    Boxdata:"vat/",
    Upload:"vat/",
    Refresh:"vat/"
  }
};
