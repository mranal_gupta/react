import { handleActions } from "redux-actions";
import { OBLIGATION_ID } from "../Action/actionType";
const intialState = {
  obligation: {}
};

export default handleActions(
  {
    [OBLIGATION_ID]: (state = intialState, { payload }) => {
      return {
        ...state,
        obligation: { ...state.obligation, payload }
      };
    }
  },
  intialState
);
