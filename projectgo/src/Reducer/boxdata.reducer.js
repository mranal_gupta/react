import { handleActions } from "redux-actions";
import { BOX_DATA } from "../Action/actionType";
const intialState = {
  boxdata: {}
};

export default handleActions(
  {
    [BOX_DATA]: (state = intialState, { payload }) => {
      return {
        ...state,
        boxdata: { ...state.boxdata, payload }
      };
    }
  },
  intialState
);
