import { handleActions } from "redux-actions";
import { LIST_DETAILS } from "../Action/actionType";
const intialState = {
  listData: {}
};

export default handleActions(
  {
    [LIST_DETAILS]: (state = intialState, { payload }) => {
      return {
        ...state,
        listData: { ...state.listData, payload }
      };
    }
  },
  intialState
);
