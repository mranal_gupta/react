import { handleActions } from "redux-actions";
import { WELCOME_USER } from "../Action/actionType";
const intialState = {
  welcomeuser: []
};

export default handleActions(
  {
    [WELCOME_USER]: (state = intialState, { payload }) => {
      return {
        ...state,
        welcomeuser: { ...state.welcomeuser, payload }
      };
    }
  },
  intialState
);
