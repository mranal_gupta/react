import {handleActions} from "redux-actions";
import {MTD_LIST} from "../Action/actionType";
const intialState = {
    mtdData: {}
};

export default handleActions({
    [MTD_LIST]: (state = intialState, {payload}) => {
        return ({
            ...state,
            mtdData:{...state.mtdData,payload}
             })}

}, intialState);