import { handleActions } from "redux-actions";
import { PAYMENT_METHOD } from "../Action/actionType";
const intialState = {
  payment: {}
};

export default handleActions(
  {
    [PAYMENT_METHOD]: (state = intialState, { payload }) => {
      return {
        ...state,
        payment: { ...state.payment, payload }
      };
    }
  },
  intialState
);
