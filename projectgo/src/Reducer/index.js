import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";
import userReducer from "../Reducer/login.reducer";
import signupReducer from "../Reducer/signup.reducer";
import listReducer from "../Reducer/list.reducer";
import adduserReducer from "../Reducer/adduser.reducer";
import forgetpassReducer from "../Reducer/forgetpass.reducer";
import changepasskeyReducer from "../Reducer/changepassword.reducer";
import dashboardReducer from "../Reducer/dashboard.reducer";
import mtddataReducer from "../Reducer/mtd.reducer";
import welcomeReducer from "../Reducer/welcome.reducer";
import liabilitiesReducer from "../Reducer/liabilities.reducer";
import subscriptionReducer from "../Reducer/subscription.reducer";
import deleteReducer from "../Reducer/deletevalue.reducer";
import paymentReducer from "../Reducer/payment.reducer";
import obligartionReducer from "../Reducer/obligation.reducer";
import boxdataReducer from "../Reducer/boxdata.reducer";
import fileuploadReducer from "../Reducer/fileupload.reudcer";
import refreshfileReducer from "../Reducer/refresh.reducer";


const Rootreducer = combineReducers({

  form: reduxFormReducer,
  user: userReducer,
  signup: signupReducer,
  list: listReducer,
  adduser: adduserReducer,
  forgetpass: forgetpassReducer,
  changekey: changepasskeyReducer,
  dashboard: dashboardReducer,
  mtddata: mtddataReducer,
  welcome: welcomeReducer,
  liabilities: liabilitiesReducer,
  subscription: subscriptionReducer,
  deletevalue: deleteReducer,
  paymentdata: paymentReducer,
  obligationdata: obligartionReducer,
  boxdata:boxdataReducer,
  fileupload:fileuploadReducer,
  refreshfile:refreshfileReducer
  
});

export default Rootreducer;
