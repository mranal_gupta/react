import { handleActions } from "redux-actions";
import { LIABI_LIST } from "../Action/actionType";
const intialState = {
  liablist: {}
};

export default handleActions(
  {
    [LIABI_LIST]: (state = intialState, { payload }) => {
      return {
        ...state,
        liablist: { ...state.liablist, payload }
      };
    }
  },
  intialState
);
