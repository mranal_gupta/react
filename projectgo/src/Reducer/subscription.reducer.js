import { handleActions } from "redux-actions";
import { SUBS_DETAILS } from "../Action/actionType";
const intialState = {
  subsData: {}
};

export default handleActions(
  {
    [SUBS_DETAILS]: (state = intialState, { payload }) => {
      return {
        ...state,
        subsData: { ...state.subsData, payload }
      };
    }
  },
  intialState
);
