import { handleActions } from "redux-actions";
import { REFRESH_FILE } from "../Action/actionType";
const intialState = {
  refreshdata: {}
};

export default handleActions(
  {
    [REFRESH_FILE]: (state = intialState, { payload }) => {
      return {
        ...state,
        refreshdata: { ...state.refreshdata, payload }
      };
    }
  },
  intialState
);
