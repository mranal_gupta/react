import {handleActions} from "redux-actions";
import {AUTH_START} from "../Action/actionType";
const intialState ={
currentUser:{}
};

export default handleActions({
    [AUTH_START]:(state=intialState,{payload})=>{

        return ({
   ...state,
   currentUser:{...state.currentUser,payload}
    })}
},intialState);
