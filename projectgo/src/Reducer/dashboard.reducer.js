import {handleActions} from "redux-actions";
import {DASHBOARD_DATA} from "../Action/actionType";
const intialState = {
    dashboardData: {}
};

export default handleActions({
    [DASHBOARD_DATA]: (state = intialState, {payload}) => {
        return ({
            ...state,
            dashboardData:{...state.dashboardData,payload}
             })}

}, intialState);
