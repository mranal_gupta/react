import { handleActions } from "redux-actions";
import { DELETE_VALUE } from "../Action/actionType";
const intialState = {
  deletevalue: {}
};

export default handleActions(
  {
    [DELETE_VALUE]: (state = intialState, { payload }) => {
      return {
        ...state,
        deletevalue: { ...state.deletevalue, payload }
      };
    }
  },
  intialState
);
