import { handleActions } from "redux-actions";
import { FILE_UPLOAD } from "../Action/actionType";
const intialState = {
  fileupload: {}
};

export default handleActions(
  {
    [FILE_UPLOAD]: (state = intialState, { payload }) => {
      return {
        ...state,
        fileupload: { ...state.fileupload, payload }
      };
    }
  },
  intialState
);
