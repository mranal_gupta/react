import React from "react";
import "../Css/Obligation.css";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "start_date",
    text: "Period Start"
  },
  {
    dataField: "end_date",
    text: "Period End"
  },
  {
    dataField: "due_date",
    text: "Due"
  },
  {
    dataField: "received_date",
    text: "Submitted"
  },
  {
    dataField: "hmrc_status_display",
    text: "Status"
  },
  {
    dataField: "Action",
    text: "Action",
    formatter: (cellContent, row) => {
      return (
        <div class="checkbox disabled">
          <button className="btn-vat-outline outline-vat btn-view rounded-circle">
            <i className="icon-view" />
          </button>
        </div>
      );
    }
  }
];

const rowEvents = {
  onClick: (e, row, rowIndex) => {
    localStorage.setItem("meterId", row.id);
    window.location.href = `/list/${row.id}/Vatdetails/${row.id}`;
  }
};

const getobli = props => {
  const data =
    props &&
    props.mtdData &&
    props.mtdData.mtddata &&
    props.mtdData.mtddata.mtdData &&
    props.mtdData.mtddata.mtdData.payload &&
    props.mtdData.mtddata.mtdData.payload.payload &&
    props.mtdData.mtddata.mtdData.payload.payload.results.length > 0
      ? props.mtdData.mtddata.mtdData.payload.payload.results
      : [];
  return data;
};

const Obligation = props => {
  return (
    <div className="container pt-5">
      <div className="row pt-4">
        <div className="col-12">
          <div className="card w-100">
            <div className="card-header p-4">
              <div className="d-table w-100">
                <div className="d-table-cell align-middle">
                  <h5>OBLIGATIONS</h5>
                </div>
                <div className="d-table-cell text-right">
                  <button
                    className="btn crd-hd-btn mr-sm-3"
                    onClick={props.deleteUsersValue}
                  >
                    <i className="icon-refresh" />
                    &nbsp; REFRESH
                  </button>
                </div>
              </div>
            </div>

            <div className="card-body px-4">
              <div className="table-responsive">
                <BootstrapTable
                  keyField="id"
                  data={getobli(props)}
                  rowEvents={rowEvents}
                  columns={columns}
                  bordered={false}
                  noDataIndication={"no results found"}
                  hover
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Obligation;
