import React from "react";
import "../Css/List.css";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "business_name",
    text: "Business Name"
  },

  {
    dataField: "owner_name",
    text: "Owner Name"
  },
  {
    dataField: "vrn",
    text: "Product Name"
  },
  {
    dataField: "due_date",
    text: "Product Name"
  },
  {
    dataField: "idd",
    formatter: (cellContent, props, row) => {
      return (
        <div className="checkbox disabled">
          <button className="btn-outline outline-vat btn-view rounded-circle">
            <i className="icon-edit" />
          </button>
          <button
            className="btn-outline outline-vat btn-view rounded-circle"
            onClick={() => {
              props.deleteUsersValue(row._id);
            }}
          >
            <i className="icon-recycling-bin" />
          </button>
        </div>
      );
    }
  }
];

const getlistvalue = props => {
  const data =
    props &&
    props.listvalue &&
    props.listvalue.listValue &&
    props.listvalue.listValue.listData &&
    props.listvalue.listValue.listData.payload &&
    props.listvalue.listValue.listData.payload.payload &&
    props.listvalue.listValue.listData.payload.payload.results.length > 0
      ? props.listvalue.listValue.listData.payload.payload.results
      : [];

  return data;
};

const rowEvents = {
  onClick: (e, row, rowIndex) => {
    localStorage.setItem("parameterId", row.id);
    window.location.href = `/list/${row.id}`;
  }
};

const List = props => {
  return (
    <div className="container pt-4 dash">
      <BootstrapTable
        keyField="id"
        data={getlistvalue(props)}
        columns={columns}
        noDataIndication={"no results found"}
        rowClasses="table vat-table vat-table-light"
        rowEvents={rowEvents}
        hover
      />
    </div>
  );
};

export default List;
//
