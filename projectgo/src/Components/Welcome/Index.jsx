import React from "react";
import "../Css/Welcome.css";

const getvat = props => {
  const data =
    props &&
    props.data &&
    props.data.welcomedata &&
    props.data.welcomedata.welcomeuser &&
    props.data.welcomedata.welcomeuser.payload &&
    props.data.welcomedata.welcomeuser.payload.payload
      ? props.data.welcomedata.welcomeuser.payload.payload
      : {};
  return data;
};

const Welcome = props => {
  return (
    <div>
      <li className="nav-item dropdown">
        <a
          className="nav-link dropdown-toggle"
          href="/login"
          id="navbardrop"
          data-toggle="dropdown"
        >
          <b>Welcome</b> {getvat(props).first_name} {getvat(props).last_name}
        </a>
        <div className="dropdown-menu">
          <a
            className="dropdown-item"
            href="/login"
            onClick={props.handleChange}
          >
            <i className="fa fa-sign-out fa-fw" /> Logout
          </a>
        </div>
      </li>
    </div>
  );
};

export default Welcome;
