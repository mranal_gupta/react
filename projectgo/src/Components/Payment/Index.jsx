import React from "react";
import "../Css/Addbutton.css";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "amount",
    text: "Amount"
  },
  {
    dataField: "date_paid",
    text: "Date-paid"
  }
];

const getobli = props => {
  const data =
    props &&
    props.payment &&
    props.payment.payment &&
    props.payment.payment.payload &&
    props.payment.payment.payload.results &&
    props.payment.payment.payload.results.length > 0
      ? props.payment.payment.payload.results
      : [];
  return data;
};

const Payment = props => {
  console.log("props-payment", props);
  return (
    <div>
      <div className="col-sm-4 my-5">
        <div className="card w-100 h-100">
          <div className="card-header p-4">
            <h5>PAYMENTS</h5>
          </div>
          <div className="card-body py-0 px-4">
            <div className="progress-group py-4">
              <BootstrapTable
                keyField="id"
                data={getobli(props)}
                columns={columns}
                bordered={false}
                noDataIndication={"no results found"}
                hover
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
