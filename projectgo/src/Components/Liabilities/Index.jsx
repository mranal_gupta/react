import React from "react";
import "../Css/List.css";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "from_date",
    text: "Period Start"
  },
  {
    dataField: "to_date",
    text: "Period End"
  },
  {
    dataField: "charge_type",
    text: "Type"
  },
  {
    dataField: "orstanding_amount",
    text: "Amount Due"
  },
  {
    dataField: "original_amount",
    text: "Amount Paid"
  },
  {
    dataField: "due",
    text: "Due Date"
  }
];

const getobli = props => {
  const data =
    props &&
    props.ldata &&
    props.ldata.results.length > 0
      ? props.ldata.results
      : [];
  return data;
};

const Liabilities = props => {
  return (
    <div className="col-12">
      <div className="card w-100 mt-5">
        <div className="card-header p-4">
          <h5>LIABILITIES</h5>
        </div>
        <div className="card-body px-4">
          <div className="table-responsive">
            <table className="table vat-table text-nowrap">
              <BootstrapTable
                keyField="id"
                data={
                  getobli(props)
                }
                columns={columns}
                bordered={false}
                noDataIndication={"no results found"}
                hover
              />
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Liabilities;
