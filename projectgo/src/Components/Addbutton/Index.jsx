import React from 'react';
import '../Css/Addbutton.css';


const AddButton = props => {
    return (
        <div>
         <button className="btn btn-vat-dark mr-md-2" onClick={props.handleChange}> Add New &nbsp; 
         <i className="icon-add-tax"></i></button>
        </div>
    )
}

export default AddButton;