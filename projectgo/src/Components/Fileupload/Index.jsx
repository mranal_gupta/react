import React from "react";

const Fileupload = props => {
  return (
    <div>
      <div class="col-12 mt-5">
        <div class="upload-area text-center p-4">
          <img
            src="../../src/assets/img/upload.svg"
            width="14%"
            class="upload-img my-4"
          />
          <p class="upload-bld-msg">
            <b>Drag and Drop Files here</b>
          </p>
          <p class="upload-lit-msg">OR</p>
          <button class="btn btn-upload-dark" onClick={props.pictureChange}  >
            <i class="icon-add" />&nbsp; Add Files
          </button>
        </div>
      </div>
    </div>
  );
};

export default Fileupload;
