import React from "react";
import "../Css/List.css";

const Listheader = props => {
  return (
    <div className="col-sm-12 pt-4">
      <div className="px-3 px-md-5 bg-sub">
          <div className="col-sm-12">
            <div className="table-responsive">
              <table className="vat-table vat-table-light">
                <thead>
                  <tr>
                    <th />
                    <th>Business Name</th>
                    <th>Owner Name</th>
                    <th>Vat Number</th>
                    <td className="indicate-row">
                      <span className="indicat bg-mtd mr-2" />MTD
                      <span className="indicat bg-trad mx-2" />Traditional
                    </td>
                  </tr>
                </thead>
                <tbody />
              </table>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Listheader;
