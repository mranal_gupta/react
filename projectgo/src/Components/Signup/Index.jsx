import React from "react";
import { Field, reduxForm } from "redux-form";
import { required } from "redux-form-validators";
import "../Css/Signup.css";

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <div>
        <input {...input} type={type} />
        {touched &&
          ((error && <div style={{ color: "red" }}>{error}</div>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  </div>
);

const Signup = props => {
  return (
    <div className="container py-1">
      <div className="row pt-2">
        <div className="col-12">
          <div className="card w-100">
            <div className="card-header py-4">
              <h5>SIGN UP FORM</h5>
            </div>

            <div className="card-body justify-content-center">
              <div className="row justify-content-center align-items-center">
                <div className="col col-sm-6 col-md-6 col-lg-6 col-xl-6 pt-4">
                  <form onSubmit={props.manageSubmit}>
                    <label>Name</label>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <Field
                          name="first_name"
                          type="text"
                          component={renderField}
                          placeholder="First Name"
                          onChange={props.manageChange}
                          validate={[
                            required({ message: "First Name is Required" })
                          ]}
                        />
                      </div>
                      <div className="form-group col-md-6">
                        <Field
                          name="last_name"
                          type="text"
                          component={renderField}
                          placeholder="Last Name"
                          onChange={props.manageChange}
                          validate={[
                            required({ message: "Second Name is Required" })
                          ]}
                        />
                      </div>
                    </div>

                    <div className="form-group">
                      <label>User Type</label>
                      <span>
                        <i className="fa fa-question-circle-o fa-fw" />
                      </span>
                      <div>
                        <Field
                          name="user_type"
                          type="user"
                          component="select"
                          onChange={props.manageChange}
                          validate={[required({ message: "Required" })]}
                        >
                          <option />
                          <option value= {1} >Single</option>
                          <option value={2}>Multiple</option>
                          <option value={3}>Agent</option> ))}
                        </Field>
                      </div>
                    </div>

                    <div className="form-group">
                      <Field
                        name="email"
                        type="email"
                        label="Email"
                        onChange={props.manageChange}
                        component={renderField}
                        validate={[
                          email,
                          required({
                            message: "please enter your email address"
                          })
                        ]}
                      />
                    </div>

                    <span className="form-group">
                        <Field
                          name="password"
                          type="password"
                          label="Password"
                          component={renderField}
                          onChange={props.manageChange}
                          validate={[
                            required({ message: "Please enter a password" })
                          ]}
                        />
                      </span>
                      <label >
                          <button className="btn" type="button">
                            <i className="fa fa-eye-slash" aria-hidden="true" />
                          </button>
                        </label>
                   
                    <div className="form-group text-center">
                      <p>
                        <b>
                          By Clicking the sign up button, you agree to our{" "}
                          <a href="#T&C">Terms and Conditions.</a>
                        </b>
                      </p>
                      <button
                        type="submit"
                        className="btn btn-vat-light mt-4 px-5"
                        disabled={props.invalid}
                      >
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default reduxForm({
  form: "signup"
})(Signup);
