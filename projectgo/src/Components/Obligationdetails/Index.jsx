import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "start_date",
    text: "Period Start"
  },
  {
    dataField: "end_date",
    text: "Period End"
  },
  {
    dataField: "due_date",
    text: "Due"
  },
  {
    dataField: "received_date",
    text: "Submitted"
  },
  {
    dataField: "hmrc_status_display",
    text: "Status"
  }
];

const getobli = props => {
  const data =
    props &&
    props.payment &&
    props.payment.obligation &&
    props.payment.obligation.payload &&
    props.payment.obligation.payload.length > 0
      ? props.payment.obligation.payload
      : [];
  return data;
};

const ObligationDetails = props => {
  console.log(props);
  return (
    <div className="container pt-5">
      <div className="row pt-4">
        <div className="col-12">
          <div className="card w-100">
            <div className="card-header p-4">
              <div className="d-table w-100">
                <div className="d-table-cell align-middle">
                  <h5>OBLIGATIONS DETAILS</h5>
                </div>
              </div>
            </div>

            <div className="card-body px-4">
              <div className="table-responsive">
                <BootstrapTable
                  keyField="id"
                  data={getobli(props)}
                  columns={columns}
                  bordered={false}
                  noDataIndication={"no results found"}
                  hover
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ObligationDetails;
