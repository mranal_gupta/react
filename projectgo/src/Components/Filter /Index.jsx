import React from "react";
import "../Css/Filter.css";

const Filter = props => {
  return (
    <div className="col-sm-12 pt-4">
      <div className="py-4 px-3 px-md-5 bg-sub">
        <form>
          <div className="form-row">
            <div className="col-10 col-md-3">
              <input
                type="text"
                className="form-control mr-3"
                placeholder="Search with Keyword"
              />
            </div>
            <div className="col-2 col-md-1">
              <button className="btn-search rounded-circle">
                <i className="icon-search" />
              </button>
            </div>
            <div className="col-12 text-center text-md-left col-md-5 mt-3 mt-md-0">
              <button className="btn btn-small btn-outline mr-4">MTD</button>
              <button className="btn btn-small btn-vat mr-4">Traditional</button>
            </div>
            <div className="col-12 text-center text-md-right col-md-3 mt-3 mt-md-0">
              <button className="btn btn-outline ml-sm-auto">
                <i className="icon-filter" />
                &nbsp; Clear Filters
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Filter;
