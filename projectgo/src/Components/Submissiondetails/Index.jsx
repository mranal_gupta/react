import React from "react";


const getvat = props => {
const data = props && props.boxitem && props.boxitem.boxdata && props.boxitem.boxdata.payload ? props.boxitem.boxdata.payload : {};
return data;
};


const SubmissionDetails = props => {
console.log("submission props", props);
return (
<div>
<div className="col-12 mt-5">
<div className="card card-disabled w-100">
<div className="card-header p-4">
<h5>SUBMISSION DETAILS</h5>
</div>
<div className="card-body p-0">
<table className="table table-submit m-0">
<tbody>
<tr>
<th>01</th>
<td>
VAT due in this Period on Sales and other outputs{" "}
<b>VatDueSales</b>
</td>
<td>
<input type="number"
value={
getvat(props).vat_due_sales
}
disabled=""/>
</td>
</tr>
<tr>
<th>02</th>
<td>
VAT due in this Period on acquisitions from other EC Member
            States
<b>VatDueAcquisitions</b>
</td>
<td>
<input type="number"
value={
getvat(props).vat_due_acquisition
}
disabled=""/>
</td>
</tr>
<tr>
<th>03</th>
<td>
Total VAT due (Sum of boxes 1 and 2)
<b>TotalVatDue</b>
</td>
<td>
<input type="number"
value={
getvat(props).total_vat_due
}
disabled=""/>
</td>
</tr>
<tr>
<th>04</th>
<td>
VAT reclaimed in this period on purchases and other inputs
            (including acquisitions from the EC)
<b>VatReclaimedCurrPeriod</b>
</td>
<td>
<input type="number"
value={
getvat(props).vat_reclaimed_curr_period
}
disabled=""/>
</td>
</tr>
<tr>
<th>05</th>
<td>
Net VAT to be paid to Customs or reclaimed by you
            (Difference between boxes 3 and 4)
<b>NetVatDue</b>
</td>
<td>
<input type="number"
value={
getvat(props).net_vat_due
}
disabled=""/>
</td>
</tr>
<tr>
<th>06</th>
<td>
Total value of Sales and all other outputs excluding any
            VAT. (Including your box 8 figure)
<b>TotalValueSalesExVAT</b>
</td>
<td>
<input type="number"
value={
getvat(props).total_value_sales_ex_vat
}
disabled=""/>
</td>
</tr>
<tr>
<th>07</th>
<td>
Total value of Purchases and all other inputs excluding any
            VAT. (Including your box 9 figure)
<b>TotalValuePurchasesExVAT</b>
</td>
<td>
<input type="number"
value={
getvat(props).total_value_purchases_ex_vat
}
disabled=""/>
</td>
</tr>
<tr>
<th>08</th>
<td>
Total value of all supplies of goods and related services,
            excluding any VAT, to other EC Member States{" "}
<b>TotalValueGoodsSuppliedExVAT</b>
</td>
<td>
<input type="number"
value={
getvat(props).total_value_goods_supplied_ex_vat
}
disabled=""/>
</td>
</tr>
<tr>
<th>09</th>
<td>
Total value of all acquisitions of goods and related
            services, excluding any VAT, from other EC Member States{" "}
<b>TotalAcquisitionsExVAT</b>
</td>
<td>
<input type="number"
value={
getvat(props).total_acquisitions_ex_vat
}
disabled=""/>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
);
};

export default SubmissionDetails;
