import React from "react";
import "../Css/Setting.css";
import { Link } from "react-router-dom";

const Setting = props => {
  return (
    <div className="container-fluid">
      <div className="row h-100">
        <aside className="col-12 col-md-3 col-lg-3 col-xl-2 p-0 bg-white nav-side">
          <nav className="navbar navbar-expand navbar-light bg-white flex-md-column flex-row align-items-start py-md-5 px-0">
            <div className="collapse navbar-collapse w-100">
              <ul className="flex-md-column flex-row navbar-nav w-100 justify-content-center">
                <li className="nav-item px-4 p-md-2">
                  <Link className="nav-link text-nowrap" to="/editpro" >
                    <i className="icon-edit_profile" />
                    <span className="d-none d-md-inline pl-md-2">
                      Edit Profile
                    </span>
                  </Link>
                </li>
                <li className="nav-item px-4 p-md-2">
                  <Link className="nav-link text-nowrap" to="/ChangePass">
                    <i className="icon-key" />
                    <span className="d-none d-md-inline pl-md-2">
                      Change Password
                    </span>
                  </Link>
                </li>
                <li className="nav-item px-4 p-md-2">
                  <Link className="nav-link text-nowrap" to="/Subscription">
                    <i className="icon-forms" />
                    <span className="d-none d-md-inline pl-md-2">
                      My Subscription
                    </span>
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
        </aside>
      </div>
    </div>
  );
};
export default Setting;
