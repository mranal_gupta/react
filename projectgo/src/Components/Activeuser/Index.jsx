import React from "react";
import '../Css/Activeuser.css';


const ActiveUser = props => {
  return (
    <div>
      <div>
        <div className="col-12 my-5">
          <div id="accordion">
            <div className="card">
              <div className="card-header">
                <div className="d-table w-100">
                  <div className="d-table-cell align-middle">
                    <h5>ACTIVE USERS</h5>
                  </div>
                  <div className="d-table-cell text-right">
                    <button
                      className="btn crd-hd-btn mr-3"
                      data-toggle="modal"
                      data-target="#myModal"
                    >
                      Invite
                    </button>
                  </div>
                  <div className="d-table-cell text-center align-middle">
                    <a
                      className="collapsed card-link"
                      data-toggle="collapse"
                      href="#collapseOne"
                    >
                      <i className="icon" />
                    </a>
                  </div>
                </div>
              </div>
              <div id="collapseOne" className="collapse" data-parent="#accordion">
                <div className="card-body d-table w-100 p-0">
                  <div className="d-table-row card-bg-row">
                    <div className="d-table-cell pl-4 py-2">
                      <label className="card-lable pr-3 m-0">Email</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                      <br />
                      <label className="card-lable pr-3 m-0">Role</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                    </div>
                    <div className="d-table-cell text-center text-lg-right align-middle pr-4">
                      <button className="btn btn-outline mr-0 mr-lg-4">
                        <i className="icon-refresh" />
                        &nbsp; Resend
                      </button>
                      <button className="btn btn-outline outline-dark mt-4 mt-lg-0">
                        <i className="icon-refresh" />
                        &nbsp; Revoke
                      </button>
                    </div>
                  </div>
                  <div className="d-table-row card-bg-row">
                    <div className="d-table-cell pl-4 py-2">
                      <label className="card-lable pr-3 m-0">Email</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                      <br />
                      <label className="card-lable pr-3 m-0">Role</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                    </div>
                    <div className="d-table-cell text-center text-lg-right align-middle pr-4">
                      <button className="btn btn-outline mr-0 mr-lg-4">
                        <i className="icon-refresh" />
                        &nbsp; Resend
                      </button>
                      <button className="btn btn-outline outline-dark mt-4 mt-lg-0">
                        <i className="icon-refresh" />
                        &nbsp; Revoke
                      </button>
                    </div>
                  </div>
                  <div className="d-table-row card-bg-row">
                    <div className="d-table-cell pl-4 py-2">
                      <label className="card-lable pr-3 m-0">Email</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                      <br />
                      <label className="card-lable pr-3 m-0">Role</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                    </div>
                    <div className="d-table-cell text-center text-lg-right align-middle pr-4">
                      <button className="btn btn-outline mr-0 mr-lg-4">
                        <i className="icon-refresh" />
                        &nbsp; Resend
                      </button>
                      <button className="btn btn-outline outline-dark mt-4 mt-lg-0">
                        <i className="icon-refresh" />
                        &nbsp; Revoke
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>




            <div className="card">
              <div className="card-header">
                <div className="d-table w-100">
                  <div className="d-table-cell align-middle">
                    <h5>INVITED USERS</h5>
                  </div>
                  <div className="d-table-cell text-right">
                    <button className="btn crd-hd-btn invisible">Share</button>
                  </div>
                  <div className="d-table-cell text-center align-middle">
                    <a
                      className="card-link"
                      data-toggle="collapse"
                      href="#collapseTwo"
                    >
                      <i className="icon" />
                    </a>
                  </div>
                </div>
              </div>

              <div
                id="collapseTwo"
                className="collapse show"
                data-parent="#accordion"
              >
                <div className="card-body d-table w-100 p-0">
                  <div className="d-table-row card-bg-row">
                    <div className="d-table-cell pl-4 py-2">
                      <label className="card-lable pr-3 m-0">Email</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                      <br />
                      <label className="card-lable pr-3 m-0">Role</label>&nbsp;
                      <p className="card-text d-inline">Lorem ipsum</p>
                    </div>
                    <div className="d-table-cell text-center text-sm-right align-middle pr-4">
                      <button className="btn btn-outline mr-0 mr-sm-4">
                        <i className="icon-refresh" />
                        &nbsp; Resend
                      </button>
                      <button className="btn btn-outline outline-dark mt-4 mt-sm-0">
                        <i className="icon-refresh" />
                        &nbsp; Revoke
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="myModal">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header py-2">
              <h4 className="modal-title">SHARE / INVITE</h4>
              <button type="button" className="icon-close" data-dismiss="modal" />
            </div>
            <div className="modal-body">
              <div className="form-row d-flex">
                <div className="col-8">
                  <input
                    type="email"
                    className="form-control mr-3"
                    placeholder="loremipsum@xyz.com"
                  />
                </div>
                <div className="col-4 text-center align-self-center">
                  <button className="btn btn-small btn-vat px-4">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ActiveUser;
