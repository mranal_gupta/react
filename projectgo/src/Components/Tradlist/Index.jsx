import React from "react";
import '../Css/Tradelist.css';


const TradList = (props) => {

return (
    <div className="container pt-5">
    <div className="row pt-4">
<div className="col-12 my-5">
<div id="accordion">
<div className="card">
<div className="card-header">
<div className="d-table w-100">
<div className="d-table-cell align-middle">
<h5>ACTIVE USERS</h5></div>
<div className="d-table-cell text-right">
<button className="btn crd-hd-btn mr-3" data-toggle="modal" data-target="#myModal">Share</button></div>
<div className="d-table-cell text-center align-middle">
<a className="collapsed card-link" data-toggle="collapse" href="#collapseOne">
<i className="icon"></i></a></div>
</div>
</div>
<div id="collapseOne" className="collapse" data-parent="#accordion">
<div className="card-body d-table w-100 p-0">
<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>

<div className="d-table-cell text-center text-lg-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-lg-4"><i className="icon-refresh">
</i>&nbsp; Resend</button>
<button className="btn btn-outline outline-dark mt-4 mt-lg-0">
<i className="icon-refresh">
</i>&nbsp; Revoke</button></div>
</div>

<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>

<div className="d-table-cell text-center text-lg-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-lg-4">
<i className="icon-refresh"></i>&nbsp; Resend</button>
<button className="btn btn-outline outline-dark mt-4 mt-lg-0">
<i className="icon-refresh"></i>&nbsp; Revoke</button>
</div>

</div>l
<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>

<div className="d-table-cell text-center text-lg-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-lg-4">
<i className="icon-refresh"></i>&nbsp; Resend</button>
<button className="btn btn-outline outline-dark mt-4 mt-lg-0">
<i className="icon-refresh"></i>&nbsp; Revoke</button></div>
</div>
</div>
</div>
</div>
<div className="card">
<div className="card-header">
<div className="d-table w-100">
<div className="d-table-cell align-middle">
<h5>INVITED USERS</h5></div>
<div className="d-table-cell text-right">
<button className="btn crd-hd-btn invisible">Share</button></div>
<div className="d-table-cell text-center align-middle">
<a className="card-link" data-toggle="collapse" href="#collapseTwo">
<i className="icon"></i></a></div>
</div>
</div>
<div id="collapseTwo" className="collapse show" data-parent="#accordion">
<div className="card-body d-table w-100 p-0">
<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>
<div className="d-table-cell text-center text-sm-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-sm-4"><i className="icon-refresh">
</i>&nbsp; Resend</button><button className="btn btn-outline outline-dark mt-4 mt-sm-0">
<i className="icon-refresh"></i>&nbsp; Revoke</button></div>
</div>
<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>
<div className="d-table-cell text-center text-sm-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-sm-4">
<i className="icon-refresh">
</i>&nbsp; Resend</button>
<button className="btn btn-outline outline-dark mt-4 mt-sm-0"><i className="icon-refresh">
</i>&nbsp; Revoke</button></div>
</div>
<div className="d-table-row card-bg-row">
<div className="d-table-cell pl-4 py-2">
<label className="card-lable pr-3 m-0">Email</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p><br />
<label className="card-lable pr-3 m-0">Role</label>&nbsp;
<p className="card-text d-inline">Lorem ipsum</p>
</div>
<div className="d-table-cell text-center text-sm-right align-middle pr-4">
<button className="btn btn-outline mr-0 mr-sm-4"><i className="icon-refresh"></i>&nbsp; Resend
</button><button className="btn btn-outline outline-dark mt-4 mt-sm-0"><i className="icon-refresh">
</i>&nbsp; Revoke</button></div>
</div>
</div>
</div>
</div>
</div>
</div>

<div className="modal fade" id="myModal">
<div className="modal-dialog modal-dialog-centered">
<div className="modal-content">
<div className="modal-header py-2">
<h4 className="modal-title">SHARE / INVITE</h4>
<button type="button" className="icon-close" data-dismiss="modal"></button>
</div>
<div className="modal-body">
<div className="form-row d-flex">
<div className="col-8">
<input type="email" className="form-control mr-3" placeholder="loremipsum@xyz.com" />
</div>
<div className="col-4 text-center align-self-center">
<button className="btn btn-small btn-vat px-4">Search</button>
</div>
<div className="col-12 mt-4 ">
<div className="custom-control custom-control-inline p-0">
<label className="total-label">Share / Invite as</label>
</div>
<div className="custom-control custom-control-inline">
<ul className="nav" role="tablist">
<li className="nav-item">
<a className="modal-tab active" data-toggle="tab" href="#client" role="tab" aria-controls="home" aria-selected="true">
<div className="custom-control custom-radio custom-control-inline">
<input type="radio" className="custom-control-input" id="customRadio" name="example" value="customEx" defaultChecked />
<label className="custom-control-label" htmlFor="customRadio">Client</label>
</div>
</a>
</li>
<li className="nav-item">
<a className="modal-tab" data-toggle="tab" href="#assistant" role="tab" aria-controls="profile" aria-selected="false">
<div className="custom-control custom-radio custom-control-inline">
<input type="radio" className="custom-control-input" id="customRadio2" name="example" value="customEx" />
<label className="custom-control-label" htmlFor="customRadio2">Assistant</label>
</div>
</a>
</li>
</ul>
</div>
<div id="tab" className="btn-group" data-toggle="buttons">
<a href="#client" className="btn btn-default active" data-toggle="tab">
<div className="custom-control custom-radio custom-control-inline">
<input type="radio" className="custom-control-input" id="customRadio" name="example" value="customEx" defaultChecked />
<label className="custom-control-label" htmlFor="customRadio">Client</label>
</div>
</a>
<a href="#assistant" className="btn btn-default" data-toggle="tab">
<div className="custom-control custom-radio custom-control-inline">
<input type="radio" className="custom-control-input" id="customRadio2" name="example" value="customEx" />
<label className="custom-control-label" htmlFor="customRadio2">Assistant</label>
</div>
</a>
</div>
</div>
<div className="col-12 mt-3 tab-content">
<div className="tab-pane fade show active" id="client">
<table className="table model-table">
<thead>
<tr>
<th>#</th>
<th>Name</th>
<th>Email</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Lorem ipsum</td>
<td>loremipsum@xyz.com <button className="btn btn-small btn-vat ml-2">Share</button></td>
</tr>
</tbody>
</table>
</div>
<div className="tab-pane" id="assistant">
<p>No record found with email <b>loremipsum@xyz.com</b> <a href="#Home">Invite now</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

)
}

export default TradList;