import React from 'react';
import { Field, reduxForm } from 'redux-form';
import '../Css/Forgetpass.css';




const required = value => value ? undefined : 'Please enter your email address'
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'The email is not valid. Please check and try again' : undefined

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <label>{label}</label>
        <div>
            <div>
                <input {...input} type={type} />
                {touched &&
          ((error && <div style={{ color: "red" }}>{error}</div>) ||
            (warning && <span>{warning}</span>))}
            </div>
        </div>
    </div>

)


const Forgetpass = (props) => {
    return (
        <div className="container py-4 dash">
            <div className="row pt-5">
                <div className="col-12">
                    <div className="card w-100">
                        <div className="card-header text-center py-4">
                            <h5>To Change your password, Please provide the information below </h5>
                        </div>
                        <div className="card-body justify-content-center">
                            <div className="row justify-content-center align-items-center">
                                <div className="col col-sm-6 col-md-6 col-lg-6 col-xl-6 pt-4">
                                    <form onSubmit={props.handleSubmit}>
                                        <div className="form-group">
                                            <Field name="email" type="email" className="form-control"
                                                component={renderField} label="Email"
                                                validate={[email, required]} onChange={props.handleChange} />
                                        </div>
                                        <div className="form-group text-center">
                                            <button type="submit"  disabled={props.invalid} className="btn btn-vat-light mt-4 px-5">RESET PASSWORD</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}


export default reduxForm({
    form: 'forgetpass'
})(Forgetpass)    
