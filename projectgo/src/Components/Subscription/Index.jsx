import React from "react";
import "../Css/Subscription.css";
import BootstrapTable from "react-bootstrap-table-next";

const columns = [
  {
    dataField: "plan_name",
    text: "Plan Name"
  },
  {
    dataField: "no_of_license",
    text: "Quantity"
  },
  {
    dataField: "status",
    text: "Status"
  },
  {
    dataField: "next_renewal_date",
    text: "Renewal Date"
  },
  {
    dataField: "amount",
    text: "Amount($)"
  },
];

const getlistvalue = props => {
  const data =
    props &&
    props.data &&
    props.data.sdata &&
    props.data.sdata.subsData &&
    props.data.sdata.subsData.payload &&
    props.data.sdata.subsData.payload.payload &&
    props.data.sdata.subsData.payload.payload.results.length > 0
      ? props.data.sdata.subsData.payload.payload.results
      : [];

  return data;
};


const Subscription = props => {
  return (
    <main className="col p-md-5">
      <div className="card w-100">
        <div className="card-header py-4">
          <h5>SUBSCRIPTION DETAILS</h5>
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <BootstrapTable
              keyField="id"
              data={getlistvalue(props)}
              columns={columns}
              bordered={false}
              noDataIndication={"no results found"}
              hover
            />
          </div>
        </div>
      </div>
    </main>
  );
};

export default Subscription;
