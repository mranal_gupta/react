import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import Login from "../Container/login.container";
import Signup from "../Container/signup.container";
import DashPage from "../Container/dashboard.container";
import SettingPage from "../Container/setting.container";
import FilterPage from "../Container/filter.container";
import EditProPage from "../Container/editpro.container";
import ForgetpassPage from "../Container/forgetpass.container";
import SubscriptionPage from "../Container/subscription.container";
import ListPage from "../Container/list.container";
import Header from "../Components/Header/Index";
import NavbarPage from "../Container/navbar.container";
import TradlistPage from "../Container/tradlist.container";
import BreadcrumbPage from "../Container/breadcrumb.container";
import VatdetailsPage from "../Container/vatdetails.container";
import AddNewPage from "../Container/addnew.container";
import MtdlistPage from "../Container/mtdlist.container";
import ChangepassPage from "../Container/changepass.container";
import SetvatPage from "../Container/setvat.container";
import Listheader from "../Components/Listheader/index";
import liabilitiesPage from "../Container/liabilities.container";
import liabilities from "../Components/Liabilities/Index";
import ActiveuserPage from "../Container/activeuser.container";
import PaymentPage from "../Container/payment.container";
import FileuploadPage from "../Container/fileupload.container";
import ObligationdetailsPage from "../Container/obligationdetails.container";
import SubmissiondetailsPage from "../Container/submission.container";

const PrivateRoute = ({ component: Component }) => (
  <Route
    render={props =>
      localStorage.getItem("access_token") ? (
        <Component {...props} />
      ) : (
        <Redirect to={"/login"} />
      )
    }
  />
);

const LoginRoute = ({ component: Component }) => (
  <Route
    render={props =>
      !localStorage.getItem("username") ? (
        <Component {...props} />
      ) : (
        <Redirect to={"/DashBoard"} />
      )
    }
  />
);

export const Routing = () => {
  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/" component={DashPage} />
        <LoginRoute exact path="/Login" component={Login} />
        <Route exact path="/Signup" component={Signup} />
        <PrivateRoute exact path="/list" component={ListPage} />
        <PrivateRoute exact path="/list/:id/Vatdetails/:id" component={VatdetailsPage} />
        <PrivateRoute exact path="/Addnew" component={AddNewPage} />
        <PrivateRoute exact path="/list/:id" component={MtdlistPage} />
        <PrivateRoute exact path="/DashBoard" component={DashPage} />
        <PrivateRoute exact path="/setting" component={SettingPage} />
        <Route exact path="/filter" component={FilterPage} />
        <Route exact path="/editpro" component={EditProPage} />
        <Route exact path="/forgetpass" component={ForgetpassPage} />
        <Route exact path="/Subscription" component={SubscriptionPage} />
        <Route exact path="/Header" component={Header} />
        <Route exact path="/Navbar" component={NavbarPage} />
        <Route exact path="/Tradelist" component={TradlistPage} />
        <Route exact path="/Breadcrumb" component={BreadcrumbPage} />
        <Route exact path="/Changepass" component={ChangepassPage} />
        <Route exact path="/setvat" component={SetvatPage} />
        <Route exact path="/listheader" component={Listheader} />
        <Route exact path="/libilites" component={liabilitiesPage} />
        <Route exact path="/libiliteses" component={liabilities} />
        <Route exact path="/activeuser" component={ActiveuserPage} />
        <Route exact path="/payment" component={PaymentPage} />
        <Route exact path="/Fileupload" component={FileuploadPage} />
        <Route exact path="/obligationd" component={ObligationdetailsPage} />
        <Route exact path="/submission" component={SubmissiondetailsPage} />
      </Switch>
    </Router>
  );
};
