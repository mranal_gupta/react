import React from "react";
import "../Css/Dashboard.css";
import taxes from "../../assets/img/taxes.svg";

const getvat = props => {
  const data =
    props &&
    props.data &&
    props.data.dashData &&
    props.data.dashData.dashboardData &&
    props.data.dashData.dashboardData.payload &&
    props.data.dashData.dashboardData.payload.payload
      ? props.data.dashData.dashboardData.payload.payload
      : {};
  return data;
};

const Dash = props => {
  return (
    <div className="container">
      <div className="container pt-4 dash">
        <div className="row">
          <div className="col" />
        </div>
      </div>

      <div className="container">
        <div className="row pt-5">
          <div
            className="col-lg-4 col-md-6 pdt-sm-4"
            id="my-panel"
            onClick={props.handleChange}
            style={{ cursor: "pointer" }}
          >
            <div className="card h-90 yel">
              <div className="card-body">
                <div className="body-head">
                  <div className="card-img mr-3">
                    <img src={taxes} alt="vat" />
                  </div>
                  <h1>{getvat(props).vat_submission_fulfilled_count}</h1>
                </div>
                <div className="body-text pt-2">
                  <p>Total VAT return submitted</p>
                </div>
                <span className="card-icon clock" />
              </div>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6 pdt-md-4"
            id="my-panel"
            style={{ cursor: "pointer" }}
            onClick={props.pathChange}
          >
            <div className="card h-90 yel">
              <div className="card-body">
                <div className="body-head">
                  <div className="card-img mr-3">
                    <img src={taxes} alt="vat" />
                  </div>
                  <h1>{getvat(props).vat_submission_due_count}</h1>
                </div>
                <div className="body-text pt-2">
                  <p>VAT returns Submissions due</p>
                </div>
                <span className="card-icon share" />
              </div>
            </div>
          </div>
        </div>

        <div className="row pt-4">
          <div
            className="col-lg-4 col-md-6"
            id="my-panel"
            style={{ cursor: "pointer" }}
            onClick={props.navChange}
          >
            <div className="card h-90 pink">
              <div className="card-body">
                <div className="body-head">
                  <div className="card-img mr-3">
                    <img src={taxes} alt="vat" />
                  </div>
                  <h1>{getvat(props).purchased_license}</h1>
                </div>
                <div className="body-text pt-2">
                  <p>Total licenses purchased</p>
                </div>
                <span className="card-icon license" />
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6" id="my-panel">
            <div className="card h-90 org">
              <div className="card-body">
                <div className="body-head">
                  <div className="card-img mr-3">
                    <img src={taxes} alt="vat" />
                  </div>
                  <h1>{getvat(props).used_license_count}</h1>
                </div>
                <div className="body-text pt-2">
                  <p>Total licenses used</p>
                </div>
                <span className="card-icon license" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dash;
