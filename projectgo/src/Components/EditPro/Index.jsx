import React from "react";
import { Field, reduxForm } from "redux-form";
import { required, format, length } from "redux-form-validators";
import "../Css/Editpro.css";

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;

const renderField = ({
  input,  label, type, meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <div>
        <input {...input} type={type} />

        {touched &&
          ((error && <div style = {{ color: "red" }}> {error}</div>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  </div>
);

const EditPro = props => {
  return (
    <div className="container py-4 dash">
      <div className="row pt-5">
        <div className="card w-100">
          <div className="card-header py-3">
            <h5>EDIT PROFILE</h5>
          </div>

          <div className="card-body justify-content-center">
            <div className="row justify-content-center align-items-center">
              <form>
                <label>Name</label>
                <div className="form-row">
                  <div className="form-group col-md-6">
                    <Field
                      name="Fname"
                      type="text"
                      component={renderField}
                      placeholder="First Name"
                      onChange={props.onSetValue}
                      validate={[
                        required({ message: "  First Name is required " }),
                        format({
                          with: /^[a-z]+$/i,
                          message:
                            "please check there are no special charecter, Space and  numbers"
                        }),
                        length({
                          max: 20,
                          message:
                            "FirstName is to long. Max. charaecter is 20 "
                        })
                      ]}
                    />
                  </div>

                  <div className="form-group col-md-6">
                    <Field
                      name="Lname"
                      type="text"
                      component={renderField}
                      placeholder="Last Name"
                      onChange={props.onSetValue}
                      validate={[
                        required({ message: "  Second Name is required " }),
                        format({
                          with: /^[a-z]+$/i,
                          message:
                            "please check there are no special charecter, Space and  numbers"
                        }),
                        length({
                          max: 20,
                          message:
                            "SecondName is to long. Max. charaecter is 20 "
                        })
                      ]}
                    />
                  </div>
                </div>

                <div className="form-group">
                  <Field
                    name="email"
                    type="email"
                    label="Email"
                    onChange={props.onSetValue}
                    component={renderField}
                    validate={[
                      email,
                      required({ message: "Please enter your email address" })
                    ]}
                  />
                </div>

                <div className="form-group">
                  <Field
                    name="role"
                    type="text"
                    label="Role"
                    onChange={props.onSetValue}
                    component={renderField}
                    validate={[
                      required({ message: "Role required" }),
                      format({
                        with: /^[a-z]+$/i,
                        message:
                          "please check there are no special charecter, Space and  numbers"
                      }),
                      length({
                        max: 20,
                        message:
                          "Role is to long. Max. charaecter is 20 "
                      })
                    ]}
                  />
                </div>

                <div className="form-group text-center">
                  <button
                    type="submit"
                    className="btn btn-vat-light mt-4 px-5"
                    disabled={props.invalid}
                  >
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default reduxForm({
  form: "editPro"
})(EditPro);
