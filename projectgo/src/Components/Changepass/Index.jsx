import React from "react";
import { Field, reduxForm } from "redux-form";
import { required } from "redux-form-validators";
import "../Css/Changepass.css";


const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <div>
        <input {...input} type={type} />
        {touched &&
          ((error && <div style={{color:"red"}}>{error}</div>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  </div>
);

const Changepass = props => {
return (
<div className="container py-4 dash">
<div className="row pt-5">
<div className="col-12">
<div className="card w-100">
<div className="card-header p-4">
<h5>CHANGE PASSWORD</h5>
</div>

<div className="row justify-content-center align-items-center">
<div className="col col-sm-6 col-md-6 col-lg-6 col-xl-6 pt-4">
<form onSubmit={props.handleSubmit}>
  
<label className="form-group">
<Field
name="old_password"
type="password"
label="Old Password"
component={renderField}
validate={[required({message:"Password Required"})]}
onChange={props.handleChange}
/>
</label>
<label >
<button className="btn">
<i className="fa fa-eye-slash" aria-hidden="true" />
</button>
</label>


<label className="form-group">
<Field
name="new_password"
type="password"
label="New Password"
component={renderField}
validate={[required({message:"Password Required"})]}
onChange={props.handleChange}
/>
</label>
<label>
<button className="btn">
<i className="fa fa-eye-slash" aria-hidden="true" />
</button>
</label>




<label className="form-group">
<Field
name="confirm_Password"
type="password"
label="Confirm Password"
component={renderField}
validate={[required({message:"Password Required"})]}
onChange={props.handleChange}
/>
</label>
<label>
<button className="btn">
<i className="fa fa-eye-slash" aria-hidden="true" />
</button>
</label>


<div className="form-group text-center">
<button
type="submit"
className="btn btn-vat-light mt-4 px-5"
>
SUBMIT
</button>
</div>

</form>
</div>
</div>
</div>
</div>
</div>
</div>


    
  );
};

export default reduxForm({
  form: "changepass"
})(Changepass);
