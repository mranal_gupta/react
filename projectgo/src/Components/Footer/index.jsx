import React from 'react';
import '../Css/footer.css';

const Footer = (props) => {
	return (

		<footer className="page-footer fixed-bottom position-relative">
			<div className="text-center py-3">&copy; 2020 GoSimpleVAT, Inc. All rights reserved. All other trademarks are the
			property of their respective owners.</div>
		</footer>

	)
}

export default Footer;