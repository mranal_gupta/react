import React from "react";
import { Field, reduxForm } from "redux-form";
import { required } from "redux-form-validators";
import "../Css/Login.css";
import "bootstrap/dist/css/bootstrap.css";

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "The email is not valid. Please check and try again"
    : undefined;

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <div>
        <input {...input} type={type} />
        {touched &&
          ((error && <div style={{ color: "red" }}>{error}</div>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  </div>
);

const Login = props => {
  return (
<div className="container py-5">
<div className="row py-5">
<div className="col-9">
<div className="card w-10000">
<div className="card-header text-center py-4">
<h5>
To access your GoSimpleVAT account, please provide the
information below.
</h5>
</div>
{props.err !== "" ? (
        <div className="alert alert-danger ">{props.err}</div>
      ) : (
        ""
      )}
<div className="card-body justify-content-center align-items-center">
<div className="row justify-content-center align-items-center">
<div className="col col-sm-6 col-md-6 col-lg-6 col-xl-6 pt-4">
<form onSubmit={props.handleSubmit}>
<div className="form-group">
<Field
name="username"
type="username"
className="form-control"
component={renderField}
label="Email"
validate={[
email,
required({ message: "Please enter your email" })
]}
onChange={props.handleChange}
/>
</div>

<label >
<Field
name="password"
type="password"
className="form-control"
component={renderField}
validate={[
required({ message: "Please enter a password" })
]}
onChange={props.handleChange}
id="show_hide_password"
label="Your GoSimpleVAT Password"
/>
</label>

<div className=" center">
<button
type="submit"
className="btn btn-vat-light mt-4 px-5"
disabled={props.invalid}
>
Sign In
</button>

<div className="row form-group">
<div className="center
">
<b>
<a href="/forgetpass">Forgot Password</a>
</b>
</div>
<div className="col-sm-6 text-right">
<b>
<a href="/signup">
Not yet Registered? Sign up here
</a>
</b>
</div>
</div>
</div>
</form>
<br />
</div>
</div>
</div>
</div>
</div>
</div>
</div>
  );
};
export default reduxForm({
  form: "login"
})(Login);
