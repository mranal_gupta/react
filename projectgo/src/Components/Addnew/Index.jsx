import React from "react";
import { Field, reduxForm } from "redux-form";
import "../Css/Addnew.css";

const required = value => (value ? undefined : "Please enter Field");

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <div>
        <input {...input} type={type} />
        {touched &&
          ((error && <div style={{ color: "red" }}>{error}</div>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  </div>
);

const AddNew = props => {
  return (
    <div>
      <div className="container pt-4 dash">
        <div className="row justify-content-center align-items-center pt-5">
          <div className="col col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <form onSubmit={props.addUser}>
              <div className="form-group">
                <Field
                  name="business_name"
                  type="text"
                  label="Business Name"
                  onChange={props.userChange}
                  component={renderField}
                  className="form-control"
                  validate={[required]}
                  placeholder="User email"
                />
              </div>

              <div className="form-group">
                <Field
                  name="owner_name"
                  className="form-control"
                  label="Owner Name"
                  onChange={props.userChange}
                  component={renderField}
                  validate={[required]}
                  placeholder="Password"
                  type="text"
                />
              </div>

              <div className="form-group">
                <Field
                  name="vrn"
                  label="Vat Number"
                  className="form-control"
                  onChange={props.userChange}
                  component={renderField}
                  validate={[required]}
                  placeholder="Password"
                  type="text"
                />
              </div>
              <div className="form-group">
                <Field
                  name="sender_id"
                  label="Sender ID"
                  className="form-control"
                  onChange={props.userChange}
                  component={renderField}
                  validate={[required]}
                  placeholder="Password"
                  type="text"
                />
              </div>
              <div className="form-group">
                <Field
                  name="password"
                  label="Password"
                  className="form-control"
                  onChange={props.userChange}
                  component={renderField}
                  validate={[required]}
                  placeholder="Password"
                  type="Password"
                />
              </div>

              <div className="form-group btn-group pt-3">
                <button type="button" className="btn btn-vat-green">
                  MTD
                </button>
                <button type="button" className="btn btn-vat-green" >
                  Traditional
                </button>
              </div>
              <div className="d-flex justify-content-around pt-5">
                <div className="col-md-12 form-group d-flex justify-content-around flex-wrap">
                  <button onClick={props.pathChange} className="btn btn-vat-outline btn-md">Cancel</button>
                  <button type="submit" className="btn btn-vat btn-md mdt-md-4" >
                    Save
                  </button>
                  <button className="btn btn-vat-light btn-md mdt-md-4" disabled={props.invalid}>
                    Save & Connect
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default reduxForm({
  form: "addNew"
})(AddNew);
