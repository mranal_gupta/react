import React from 'react';
import '../Css/header.css';
import logo from "../../assets/img/logo.png";
import 'bootstrap/dist/css/bootstrap.css';
import WelcomePage from '../../Container/welcome.container';
import {Link} from "react-router-dom";


const currentPath = window.location.pathname

const Header = (props) => {
    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-vat justify-content-center">
            <div className="container">
                <div className="d-flex flex-column align-items-start w-100">
                    <div className="navbar-collapse ml-lg-0 ml-3 w-100">
                        <Link className="navbar-brand ml-lg-2 mr-auto" to="/dashboard"><img src={logo}
                                alt="logo"
                                width="30%"/></Link>
                        <ul className="navbar-nav top-nav">
                            {
                            localStorage.getItem("access_token") && localStorage.getItem("access_token").toString() != "" ? <WelcomePage/>: null
                        } </ul>

                    </div>
                </div>
            </div>
        </nav>

    )
}

export default Header;
