import React from 'react';
import '../Css/Navbar.css';
import { Link } from "react-router-dom";



const Navbar = (props) => {
	return (

<nav className="navbar navbar-expand-md navbar-dark bg-vat justify-content-center">
<div className="container">
<ul className="navbar-nav bottom-nav">


<li className="nav-item">
<Link className="nav-link" to="/DashBoard"> <i className="icon-dashboard"></i> Dashboard</Link>
</li>



<li className="nav-item">
<Link className="nav-link" to="/List" id="navbardrop">
<i className="icon-tax"></i> VAT</Link>
</li>


<li className="nav-item">
<Link className="nav-link" to="/editpro">
<i className="icon-settings"></i>Settings</Link>
</li>

</ul>
</div>
</nav>

	)
}

export default Navbar;
