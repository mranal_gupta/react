import React from "react";
import ActiveUser from "../Components/Activeuser/Index";

class ActiveuserPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ActiveUser />
      </div>
    );
  }
}
export default ActiveuserPage;
