import React, { Component } from "react";
import EditPro from "../Components/EditPro/Index";
import Setting from "../Container/setting.container";

class EditProPage extends Component {
  constructor(props) {
    super(props);
    this.state = { Fname: "", Lname: "", email: "", role: "" };
    this.onSetValue = this.onSetValue.bind(this);
  }

  onSetValue = ({ target }) => {
    const { name, value } = target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <span>
        <Setting />
        <main className="col p-md-5">
          <EditPro onSetValue={this.onSetValue} />
        </main>
      </span>
    );
  }
}

export default EditProPage;
