import React from "react";
import AddButton from "../Components/Addbutton/Index";
import { withRouter } from "react-router-dom";

class AddbuttonPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = () => {
    this.props.history.push("/Addnew");
  };
  
  render() {
    return (
      <div>
        <AddButton handleChange={this.handleChange} />
      </div>
    );
  }
}
export default withRouter(AddbuttonPage);
