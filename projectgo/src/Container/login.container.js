import React, {Component} from "react";
import Login from "../Components/login/Index";
import {signin} from "../Action/actionType";
import {connect} from "react-redux";
import "bootstrap/dist/css/bootstrap.css";
import Header from "../Components/Header/Index";
import Footer from "../Components/Footer/index";

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            client_id: "h7VEHPDA2Zyy0gadC3rkIS6XkaSZ0Gqigk8bcLOY",
            client_secret: "bAoB5XAQvbnLmdBcddSu54aFFZAszyDoQ2Rj1PU0aSFQ4NmceokRfNRIM2rN63wlQh7FeaEz9R67J8aoKhqCE3trFPW45ABhfaEdiMjw55G4KDgueK9vRs63tdJ6Vust",
            grant_type: "password",
            hidden: true,
            err:""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };
    handleSubmit = event => {
        event.preventDefault();
        const {username, password, client_id, client_secret, grant_type} = this.state;

        this.props.handleSubmit({username, password, client_id, client_secret, grant_type});
    };

    toggleShow() {
        this.setState({
            hidden: !this.state.hidden
        });
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps,"---------------------------------")
        if (this.props.user.currentUser !== nextProps.user.currentUser && nextProps.user.currentUser) {
            if (nextProps.user.currentUser.payload.error_message.common && nextProps.user.currentUser.payload.error_message.common.length > 0) {
                this.setState({err: nextProps.user.currentUser.payload.error_message.common[0]});
            } else {
                this.setState({message: nextProps.user.currentUser.payload.message});
                localStorage.setItem("username", this.state.username);
                localStorage.setItem("refresh_token", nextProps.user.currentUser.payload.payload.refresh_token);
                localStorage.setItem("access_token", nextProps.user.currentUser.payload.payload.access_token);
                this.props.history.push("/dashboard");
            }
        }
    }

    componentDidMount() {
        if (this.props.password) {
            this.setState({password: this.props.password});
        }
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="container py-3 dash">
                    <Login handleSubmit={
                            this.handleSubmit
                        }
                        handleChange={
                            this.handleChange
                        }
                        toggleShow={
                            this.toggleShow
                        }
                        handlePasswordChange={
                            this.handlePasswordChange
                        }
                        err={this.state.err}
                        />
                </div>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = state => ({user: state.user});

const mapActionToProps = {
    handleSubmit: signin
};

export default connect(mapStateToProps, mapActionToProps)(LoginPage);
