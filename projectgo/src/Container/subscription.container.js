import React from "react";
import { connect } from "react-redux";
import { subsdetails } from "../../src/Action/actionType";
import Subscription from "../Components/Subscription/Index";
import Setting from "../Container/setting.container";

class SubscriptionPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onsubsDetails();
  }

  render() {
    return (
      <div>
        <div>
          <Setting />
          <Subscription data={this.props} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  sdata: state.subscription
});

const mapActionToProps = {
  onsubsDetails: subsdetails
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(SubscriptionPage);
