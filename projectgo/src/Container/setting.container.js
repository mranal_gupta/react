import React from 'react';
import Setting from '../Components/Setting/Index';
import Header from '../Components/Header/Index';
import NavbarPage from '../Container/navbar.container';

class  SettingPage extends React.Component{
    render (){
        return(
       <div> 
           <Header />
           <NavbarPage />
        <div>
        <Setting />
        </div>		
        </div>

        )   
    }
}


export default SettingPage;
