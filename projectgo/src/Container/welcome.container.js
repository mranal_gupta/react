import React from "react";
import Welcome from "../Components/Welcome/Index";
import { connect } from "react-redux";
import {welcome} from "../../src/Action/actionType";


class WelcomePage extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        }
    
      handleChange = () => {
        localStorage.clear();
        sessionStorage.removeItem("userToken");
        sessionStorage.clear(); 
        this.props.history.replace('/login');
      };

      componentDidMount() {
        this.props.onwelcomeuser();
     }
    
    
    render() {
        return (
            <div>
          <Welcome handleChange={this.handleChange}
                     data={this.props} />
            </div>
        )
    }
}


const mapStateToProps = state => ({
  welcomedata: state.welcome
});

const mapActionToProps = {
  onwelcomeuser: welcome
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(WelcomePage);