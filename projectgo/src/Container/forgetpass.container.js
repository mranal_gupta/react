import React from 'react';
import Forgetpass from '../Components/ForgetPass/Index';
import Header from '../Components/Header/Index';
import { connect } from "react-redux";
import { forgetpass } from '../Action/actionType';


class ForgetpassPage extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            email: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
handleChange = event => { 
    this.setState({
        [event.target.name]: event.target.value 
    });
};


handleSubmit = event => {
    event.preventDefault()    
    const { email } = this.state;
    this.props.handleSubmit({
      email
    });
  };

    render() {

        return (

            <div>
                <Header />
                <div>
                    <Forgetpass
                     handleSubmit={this.handleSubmit}
                     handleChange={this.handleChange} />
                </div>
            </div>

       )
    }
}



const mapStateToProps = state => ({
    user: state.user
  });
  
  const mapActionToProps = {
    handleSubmit: forgetpass
    
  };
  
  export default connect(
    mapStateToProps,
    mapActionToProps
  )(ForgetpassPage);
