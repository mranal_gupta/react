import React from 'react';
import Dash from '../Components/Dashboard/Index';
import Header from '../Components/Header/Index';
import NavbarPage from '../Container/navbar.container';
import BreadcrumbPage from "../Container/breadcrumb.container";
import {dashboard} from "../Action/actionType";
import {connect} from "react-redux";

class DashPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.pathChange = this.pathChange.bind(this);
        this.navChange = this.navChange.bind(this);
    }
   
    handleChange = () => {
        this.props.history.push("/list");
    };

    pathChange = () => {
        this.props.history.push("/list");
    };

    navChange = () => {
        this.props.history.push("/Subscription");
    };


    componentDidMount() {
        this.props.ondashboard();

    }

    render() {

        return (
            <div>
                <Header/>
                <NavbarPage/>
                <div className="container pt-4 dash">
                    <BreadcrumbPage/>
                    <Dash handleChange={
                            this.handleChange
                        }
                        pathChange={
                            this.pathChange
                        }
                        navChange={
                            this.navChange
                        }
                        data={
                            this.props
                        }/>
                </div>
            </div>
        );

    }
}
const mapStateToProps = state => ({dashData: state.dashboard});

const mapActionToProps = {
    ondashboard: dashboard
};

export default connect(mapStateToProps, mapActionToProps)(DashPage);
