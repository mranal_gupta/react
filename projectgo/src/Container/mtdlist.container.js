import React from "react";
import MtdList from "../Components/Mtdlist/Index";
import Header from "../Components/Header/Index";
import NavbarPage from "../Container/navbar.container";
import BreadcrumbPage from "../Container/breadcrumb.container";
import Obligation from "../Components/Obligation/Index";
import { connect } from "react-redux";
import { mtdlist, refresh } from "../Action/actionType";
import LiabilitiesPage from "../Container/liabilities.container";
import PaymentPage from "../Container/payment.container";

class MtdlistPage extends React.Component {
  constructor(props) {
    super(props);
    this.onrefreshdata = this.onrefreshdata.bind(this);
  }

  componentDidMount() {
    this.props.onmtdlist();
  }

  onrefreshdata() {
    this.props.onrefreshdata();
  }

  render() {
    return (
      <div>
        <Header />
        <NavbarPage />`
        <div className="container pt-4 dash">
          <BreadcrumbPage />
          <Obligation
            mtdData={this.props}
            deleteUsersValue={this.onrefreshdata}
          />
          <LiabilitiesPage />
          <PaymentPage />
          <MtdList />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mtddata: state.mtddata,
  refresh: state.refreshfile
});

const mapActionToProps = {
  onmtdlist: mtdlist,
  onrefreshdata: refresh
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(MtdlistPage);
