import React from "react";
import SubmissionDetails from "../Components/Submissiondetails/Index";
import { connect } from "react-redux";
import { boxdata } from "../Action/actionType";

class SubmissiondetailsPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onboxdetails();
  }

  render() {
    return (
      <div>
        <SubmissionDetails {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { boxitem: state.boxdata };
};

const mapActionToProps = {
  onboxdetails: boxdata
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(SubmissiondetailsPage);
