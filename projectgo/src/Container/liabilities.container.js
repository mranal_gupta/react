import React from "react";
import Liabilities from "../Components/Liabilities/Index";
import { connect } from "react-redux";
import { liabilist } from "../Action/actionType";

class LiabilitiesPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onliablist();
  }

  render() {
    return (
      <div>
        <Liabilities {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state =>{
return ({ ldata: state.liabilities.liablist.payload });
} 

const mapActionToProps = {
  onliablist: liabilist
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(LiabilitiesPage);
