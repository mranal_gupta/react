import React from 'react';
import TradList from '../Components/Tradlist/Index';
import Header from '../Components/Header/Index';
import NavbarPage from '../Container/navbar.container';
import BreadcrumbPage from '../Container/breadcrumb.container';

class TradlistPage extends React.Component {

    render() {

        return (
            <div>
                <Header />
                <NavbarPage />
                <div className="container pt-4 dash">
                    <BreadcrumbPage />
                    <TradList />
                </div>
                
            </div>
        )
    }
}

export default TradlistPage; 
