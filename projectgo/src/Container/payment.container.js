import React from "react";
import Payment from "../Components/Payment/Index";
import { connect } from "react-redux";
import { paymentdata } from "../Action/actionType";

class PaymentPage extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.onpaymentmethod();
  }

  render() {
    return (
      <div>
        <Payment {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return ({ payment: state.paymentdata });
};

const mapActionToProps = {
  onpaymentmethod: paymentdata
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(PaymentPage);
