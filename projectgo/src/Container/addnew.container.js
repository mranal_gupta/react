import React from "react";
import { connect } from "react-redux";
import { adduser } from "../Action/actionType";
import AddNew from "../Components/Addnew/Index";
import Header from "../Components/Header/Index";
import NavbarPage from "../Container/navbar.container";
import BreadcrumbPage from "../Container/breadcrumb.container";

class AddNewPage extends React.Component {
  state = {
    business_name: "",
    owner_name: "",
    vrn: "",
    sender_id: "",
    password: "",
    submission_type: 1
  };

  userChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  pathChange = () => {
    this.props.history.push("/list");
};



  addUser = event => {
    event.preventDefault();
    const {
      business_name,
      owner_name,
      vrn,
      sender_id,
      password,
      submission_type
    } = this.state;
    this.props.handleSubmit({
      business_name,
      owner_name,
      vrn,
      sender_id,
      password,
      submission_type
    });
  };

  render() {
    return (
      <div>
        <Header />
        <NavbarPage />
        <div className="container pt-4 dash">
          <BreadcrumbPage />
          <AddNew addUser={this.addUser} userChange={this.userChange} pathChange={this.pathChange} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapActionToProps = {
  handleSubmit: adduser
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(AddNewPage);
