import React from "react";
import Navbar from "../Components/Navbar/Index";

class NavbarPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Navbar />
      </div>
    );
  }
}

export default NavbarPage;
