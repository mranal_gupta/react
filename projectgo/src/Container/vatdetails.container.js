import React from "react";
import VatDetails from "../Components/Vatdetails/Index";
import Header from "../Components/Header/Index";
import NavbarPage from "../Components/Navbar/Index";
import BreadcrumbPage from "../Container/breadcrumb.container";
import ObligationdetailsPage from "../Container/obligationdetails.container";
import SubmissiondetailsPage from "../Container/submission.container";

class VatdetailsPage extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <NavbarPage />
        <div className="container pt-4 dash">
          <BreadcrumbPage />
          <ObligationdetailsPage />
          <SubmissiondetailsPage />
          <VatDetails />
        </div>
      </div>
    );
  }
}

export default VatdetailsPage;
