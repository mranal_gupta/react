import React from "react";
import Changepass from "../Components/Changepass/Index";
import Setting from "../Container/setting.container";
import {changepassword} from '../Action/actionType';
import { connect } from "react-redux";


class ChangepassPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { old_password: "", new_password: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };


  handleSubmit = event => {
    event.preventDefault();
    const {
        old_password,
        new_password
    } = this.state;
    this.props.handleSubmit({
        new_password,
        old_password
    });
  };

  render() {
    return (
      <div>
        <Setting />
        <div>
          <Changepass
            handleSubmit={this.handleSubmit}
            handleChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
    user: state.user
  });
  
  const mapActionToProps = {
    handleSubmit: changepassword
  };
  

export default connect(
    mapStateToProps,
    mapActionToProps
  )(ChangepassPage);
  

