import React from "react";
import List from "../Components/List/Index";
import { connect } from "react-redux";
import {listDetails} from "../../src/Action/actionType";
import {valuedelete} from "../../src/Action/actionType";
import Header from "../Components/Header/Index";
import Navbar from "../Container/navbar.container";
import BreadcrumbPage from "../Container/breadcrumb.container";
import FilterPage from "../Container/filter.container";
import Listheader from "../Components/Listheader/index";
import SetvatPage from '../../src/Container/setvat.container';




class ListPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onlistDetails();
 }

 onDeleteUser(id) {
  this.props.onDeleteUser(id);
}

 
  render() {
    return (
      <div>
        <Header />
        <Navbar />
        <div className="container pt-4 dash">
          <BreadcrumbPage />
          <SetvatPage />
          <FilterPage />
          <List listvalue={this.props} deleteUsersValue={this.onDeleteUser} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  listValue: state.list
});

const mapActionToProps = {
  onlistDetails: listDetails,
  onDeleteUser: valuedelete
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(ListPage);
