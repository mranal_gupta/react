import React from 'react';
import Setvat from '../Components/Setvat/Index';

class SetvatPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            black: true
        };
        this.changeColor = this.changeColor.bind(this);
    }

    changeColor() {
this.setState({black: !this.state.black})

        }

    render() {
         
        return (

            <div>
            
            <Setvat changeColor={this.changeColor} />
            
            </div>
        )
    }
}

export default SetvatPage;