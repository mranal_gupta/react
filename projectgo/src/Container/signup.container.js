import React, { Component } from 'react';
import Signup from '../Components/Signup/Index';
import { connect } from 'react-redux';
import { signup } from '../Action/actionType';
import Header from '../Components/Header/Index';
import Footer from '../Components/Footer/index';

class SignupPage extends Component {

state = {

  first_name: '',
  last_name: '',
  email: '',
  password: '',
  user_type:''

}

manageChange = (event) => {
this.setState({ [event.target.name]: event.target.value });
}

manageSubmit = (event) => {
  event.preventDefault()
  const { first_name, last_name, email, password,user_type } = this.state;
  this.props.handleSubmit({
    first_name,
    last_name,
    email,
    password,
    user_type
  });
}
  render() {
    return (
      <div>
        <Header />
        <div className="container py-3 dash">
        <Signup manageSubmit={this.manageSubmit} manageChange={this.manageChange} />
      </div>
      <Footer />
      </div>
    );
  }  
}

const mapStateToProps = state => 
    ({
     user: state.user
})

const mapActionToProps = {
      handleSubmit: signup
}

export default  connect(mapStateToProps,mapActionToProps)(SignupPage);