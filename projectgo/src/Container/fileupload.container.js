import React from "react";
import Fileupload from "../Components/Fileupload/Index";
import { connect } from "react-redux";
import { uploadfile } from "../Action/actionType";

class FileuploadPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: ""
    };
  }

  // pictureChange(event) {
  //   event.preventDefault();
  //   const file = event.target.files;
  //   const myReader = new FileReader();
  //   myReader.onloadend = () => {
  //     this.setState({
  //       picture: myReader.result
  //     });
  //   };
  //   myReader.readAsDataURL(file);
  // }

  
  render() {
    return (
      <div>
        <Fileupload pictureChange={this.pictureChange} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  fileupload: state.fileupload
});
const mapActionsToProps = {
  onfileupload: uploadfile
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(FileuploadPage);
