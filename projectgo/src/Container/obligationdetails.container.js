import React from "react";
import ObligationDetails from "../Components/Obligationdetails/Index";
import { connect } from "react-redux";
import { obligationd } from "../Action/actionType";


class ObligationdetailsPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onoblimethod();
  }


  render() {
    return (
      <div>
        <ObligationDetails {...this.props} />
      </div>
    );
  }
}


const mapStateToProps = state => {
  return ({ payment: state.obligationdata });
};

const mapActionToProps = {
  onoblimethod: obligationd
};


export default connect(
  mapStateToProps,
  mapActionToProps
)(ObligationdetailsPage);

