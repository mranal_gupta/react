import { createStore, applyMiddleware } from 'redux';
import Rootreducer from '../Reducer/index';
import  ThunkMiddleware  from "redux-thunk";


const store = createStore(Rootreducer, applyMiddleware( ThunkMiddleware ));
export default store;