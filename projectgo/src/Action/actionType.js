import { createActions } from "redux-actions";
import userSource from "../Source/source";

export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const SET_AUTH_REDIRECT_PATH = "SET_AUTH_REDIRECT_PATH";
export const ADD_USER = "ADD_USER";
export const LIST_DETAILS = "LIST_DETAILS";
export const WELCOME_USER = "WELCOME_USER";
export const DASHBOARD_DATA = "DASHBOARD_DATA";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";
export const MTD_LIST = "MTD_LIST";
export const LIABI_LIST = "LIABI_LIST";
export const SUBS_DETAILS = "SUBS_DETAILS";
export const DELETE_VALUE = "DELETE_VALUE";
export const PAYMENT_METHOD = "PAYMENT_METHOD";
export const OBLIGATION_ID = "OBLIGATION_ID";
export const BOX_DATA = "BOX_DATA";
export const FILE_UPLOAD = "FILE_UPLOAD";
export const REFRESH_FILE = "REFRESH_FILE";

export const login = createActions(AUTH_START);
export const loginSuccess = createActions(AUTH_SUCCESS);
export const loginFail = createActions(AUTH_FAIL);
export const logout = createActions(AUTH_LOGOUT);
export const newuser = createActions(ADD_USER);
export const getListDetails = createActions(LIST_DETAILS);
export const welcomeuser = createActions(WELCOME_USER);
export const dashboardata = createActions(DASHBOARD_DATA);
export const changepass = createActions(CHANGE_PASSWORD);
export const mlist = createActions(MTD_LIST);
export const liablist = createActions(LIABI_LIST);
export const subslist = createActions(SUBS_DETAILS);
export const deletevalue = createActions(DELETE_VALUE);
export const paymentmethod = createActions(PAYMENT_METHOD);
export const obligationid = createActions(OBLIGATION_ID);
export const boxdeails = createActions(BOX_DATA);
export const fileupload = createActions(FILE_UPLOAD);
export const refreshfile = createActions(REFRESH_FILE);

export const signin = user => async dispatch => {
  const result = await userSource.signin(user);
  dispatch({ type: AUTH_START, payload: result });
};

export const signup = user => async dispatch => {
  const result = await userSource.signup(user);
  dispatch({ type: AUTH_SUCCESS, payload: result });
};

export const adduser = user => async dispatch => {
  const result = await userSource.adduser(user);
  dispatch({ type: ADD_USER, payload: result });
};

export const listDetails = listValue => async dispatch => {
  const result = await userSource.listdetails(listValue);
  dispatch({ type: LIST_DETAILS, payload: result });
};

export const forgetpass = user => async dispatch => {
  const result = await userSource.forgetpass(user);
  dispatch({ type: AUTH_FAIL, payload: result });
};

export const welcome = user => async dispatch => {
  const result = await userSource.welcome(user);
  dispatch({ type: WELCOME_USER, payload: result });
};

export const dashboard = dashData => async dispatch => {
  const result = await userSource.dashboard(dashData);
  dispatch({ type: DASHBOARD_DATA, payload: result });
};

export const changepassword = user => async dispatch => {
  const result = await userSource.changepassword(user);
  dispatch({ type: CHANGE_PASSWORD, payload: result });
};
export const mtdlist = mtddata => async dispatch => {
  const result = await userSource.mtdlist(mtddata);
  dispatch({ type: MTD_LIST, payload: result });
};

export const liabilist = ldata => async dispatch => {
  const result = await userSource.liabilist(ldata);
  dispatch({ type: LIABI_LIST, payload: result.payload });
};
export const subsdetails = user => async dispatch => {
  const result = await userSource.subsdetails(user);
  dispatch({ type: SUBS_DETAILS, payload: result });
};

export const valuedelete = user => async dispatch => {
  const result = await userSource.valuedelete(user);
  dispatch({ type: DELETE_VALUE, payload: result });
};

export const paymentdata = user => async dispatch => {
  const result = await userSource.paymentdata(user);
  dispatch({ type: PAYMENT_METHOD, payload: result.payload });
};

export const obligationd = user => async dispatch => {
  const result = await userSource.obligationd(user);
  dispatch({ type: OBLIGATION_ID, payload: result.payload });
};

export const boxdata = user => async dispatch => {
  const result = await userSource.boxdata(user);
  dispatch({ type: BOX_DATA, payload: result.payload });
};
export const uploadfile = user => async dispatch => {
  const result = await userSource.uploadfile(user);
  dispatch({ type: FILE_UPLOAD, payload: result.payload });
};
export const refresh = user => async dispatch => {
  const result = await userSource.refreshbutton(user);
  dispatch({ type: REFRESH_FILE, payload: result.payload });
};
