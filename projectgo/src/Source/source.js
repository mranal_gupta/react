import api from "../Serivce/api";
import config from "../Config/development";

export default {
  async signin(user) {
    return api.post(config.urlRoot.Signin, user);
  },

  async signup(user) {
    return api.post(config.urlRoot.Signup, user);
  },

  async adduser(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.post(config.urlRoot.Adduser, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async listdetails() {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.get(config.urlRoot.Get, {
      headers: {
        Authorization: authenticator
      }
    });
  },
  async forgetpass(user) {
    return api.post(config.urlRoot.Forget, user);
  },
  async welcome(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.get(
      config.urlRoot.Welcome,
      {
        headers: {
          Authorization: authenticator
        }
      },
      user
    );
  },
  async dashboard(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.get(
      config.urlRoot.Dashboard,
      {
        headers: {
          Authorization: authenticator
        }
      },
      user
    );
  },

  async changepassword(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.put(config.urlRoot.Changepass, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async mtdlist(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.MTD + localStorage.getItem("parameterId") + "/obligation";
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async liabilist(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Liab +
      localStorage.getItem("parameterId") +
      "/liabilities";
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async subsdetails(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    return api.get(config.urlRoot.SubsGet, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async deletevalue(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Delvalue + localStorage.getItem("parameterId");
    return api.delete(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async paymentdata(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Payment + localStorage.getItem("parameterId") + "/payment";
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async obligationd(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Obligation +
      localStorage.getItem("parameterId") +
      "/obligation/" +
      localStorage.getItem("meterId");
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async boxdata(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Boxdata +
      localStorage.getItem("parameterId") +
      "/obligation/" +
      localStorage.getItem("meterId") +
      "/boxes";
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async fileupload(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Upload +
      localStorage.getItem("parameterId") +
      "/obligation/" +
      localStorage.getItem("meterId") +
      "/upload-nine-box";
    return api.post(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  },

  async refreshbutton(user) {
    const authenticator =
      "Bearer " + localStorage.getItem("access_token").toString();
    const customeUrl =
      config.urlRoot.Refresh +
      localStorage.getItem("parameterId") +
      "/hmrc-refresh";
    return api.get(customeUrl, {
      headers: {
        Authorization: authenticator
      }
    });
  }
};
