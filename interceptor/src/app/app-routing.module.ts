import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoaderComponent } from "./components/loader.component";
import { CommonModule } from "@angular/common";
import { paths } from "./const";
import { MatButtonModule } from "@angular/material/button";

const routes: Routes = [
  { path: paths.loader, component: LoaderComponent },
];

@NgModule({
  declarations: [
    LoaderComponent,
  ],
  imports: [CommonModule, MatButtonModule, RouterModule.forRoot(routes)],
  exports: [
    LoaderComponent,
    RouterModule
  ]
})
export class AppRoutingModule { }
