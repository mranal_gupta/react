import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoaderService } from "../services/loader.service";

@Component({
  templateUrl: './loader.component.html',
  styleUrls: ["loader.component.css"],

})
export class LoaderComponent {
  response;

  constructor(private http: HttpClient, public loaderService: LoaderService) { }

  run() {
    const url = "https://172.24.125.158:3000";
    this.http.get(url).subscribe(r => (this.response = r));
  }
}
